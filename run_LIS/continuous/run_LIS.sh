#!/bin/bash
#PBS -l walltime=1:50:00
#PBS -l mem=16GB
#PBS -l ncpus=32
#PBS -j oe
#PBS -q express 
#PBS -l wd

NUWRF_DIR=/short/w35/ccc561/WRF/nuwrf
export PATH="${NUWRF_DIR}/installs/nuwrf_v9_p0:${PATH}"
source $NUWRF_DIR/raijin.cfg
module load intel-mkl/${EBVERSIONINTELMINFC}
module load arm-forge
module load totalview

# Create link to the LIS data
ln -sf /g/data/w35/LIS/LIS_PARAMS LIS_PARAMS

mpirun -np $PBS_NCPUS LIS -f lis.config
#ddt mpirun -np $PBS_NCPUS ./LIS -f lis.config
#mpirun --debug -np 8 ./LIS -f lis.config
