#Overall driver options
Running mode: 		         "retrospective"
Map projection of the LIS domain: "latlon"
Number of nests:                 1
Number of surface model types:    1
Surface model types:            "LSM"
Surface model output interval:  "30mn"
Land surface model:             "CABLE.2.3.4"
Number of met forcing sources:   1
Blending method for forcings:    "overlay"
Met forcing sources:             "PRINCETON"
Met forcing chosen ensemble member: 1
#Topographic correction method (met forcing):  "lapse-rate"
Topographic correction method (met forcing):  "none"
Enable spatial downscaling of precipitation:  0
Spatial interpolation method (met forcing):   "bilinear"
Spatial upscaling method (met forcing): "neighbor"
Temporal interpolation method (met forcing):  "linear"

#Runtime options
Forcing variables list file:               ./LIS_PARAMS/forcing_variables_prince.txt
Output forcing:                            1   #1-yes
Output parameters:                         1   #0- no
Output methodology:                        "2d gridspace"
Output model restart files:                1
Output data format:                        "netcdf"
Output naming style:                       "2 level hierarchy"
Start mode:                                "restart"
Starting year:                             2002
Starting month:                            10
Starting day:                              30
Starting hour:                             0
Starting minute:                           0
Starting second:                           0
Ending year:                               2002
Ending month:                              11
Ending day:                                02
Ending hour:                               0
Ending minute:                             0
Ending second:                             0
Undefined value:                          -9999
Output directory:                         'OUTPUT' 
Diagnostic output file:                   'lislog'
Number of ensembles per tile:              1

#The following options are used for subgrid tiling based on vegetation
Maximum number of surface type tiles per grid:     1
Minimum cutoff percentage (surface type tiles):    0.05 
Maximum number of soil texture tiles per grid:     1
Minimum cutoff percentage (soil texture tiles):    0.10
Maximum number of soil fraction tiles per grid:    1
Minimum cutoff percentage (soil fraction tiles):   0.10
Maximum number of elevation bands per grid:        1
Minimum cutoff percentage (elevation bands):       0.10
Maximum number of slope bands per grid:            1
Minimum cutoff percentage (slope bands):           0.10
Maximum number of aspect bands per grid:           1
Minimum cutoff percentage (aspect bands):          0.10

#Processor Layout	
#Should match the total number of processors used

Number of processors along x:    4
Number of processors along y:    2
Halo size along x: 0
Halo size along y: 0 

#------------------------ ROUTING -------------------------------------

Routing model:                    "none"

#------------------------RADIATIVE TRANSFER MODELS--------------------------

Radiative transfer model:   "none"

#------------------------APPLICATION MODELS---------------------------------

Number of application models: 0

#---------------------DATA ASSIMILATION ----------------------------------
#Data Assimilation Options

Number of data assimilation instances:               0
Data assimilation algorithm:                         "Direct insertion"
Data assimilation set:                               "SNODEP" 
Number of state variables:                           2
Data assimilation exclude analysis increments:       1
Data assimilation output interval for diagnostics:   "1da"  
Data assimilation number of observation types:       1 
Data assimilation output ensemble members:           0
Data assimilation output processed observations:     0
Data assimilation output innovations:                0

Bias estimation algorithm:                "none"
Bias estimation attributes file:          "none"
Bias estimation restart output frequency:
Bias estimation start mode:
Bias estimation restart file:

#Perturbation options
Perturbations start mode:                 "coldstart"
Perturbations restart output interval:    "1mo"
Perturbations restart filename:           ./LIS_DAPERT_200902282330.d01.bin

Forcing perturbation algorithm:           "none" 
Forcing perturbation frequency:           "1hr"
Forcing attributes file:                  ./forcing_attribs.txt
Forcing perturbation attributes file:     ./forcing_pert_attribs.txt

State perturbation algorithm:             "none"
State perturbation frequency:             "3hr"
State attributes file:                 ./lis_configs/noah_snow_attribs.txt
State perturbation attributes file:       ./config/noah_snow_pertattribs.txt

Observation perturbation algorithm:       "none"
Observation perturbation frequency:       "6hr"
Observation attributes file:           ./lis_configs/SNODEPobs_attribs.txt
Observation perturbation attributes file: ./config/SNODEP_pertattribs.txt


#------------------------DOMAIN SPECIFICATION--------------------------
#Definition of Running Domain
#Specify the domain extremes in latitude and longitude

Run domain lower left lat:                  25.875 
Run domain lower left lon:                -124.875
Run domain upper right lat:                 52.875
Run domain upper right lon:                -67.875
Run domain resolution (dx):                  0.25
Run domain resolution (dy):                  0.25

#The following options list the choice of parameter maps to be 
#used
Landmask data source:            "LDT"
Landcover data source:           "LDT"
Soil texture data source:        "none"
Soil fraction data source:       "LDT"
Soil color data source:          "none"
Elevation data source:           "LDT"
Slope data source:               "none"
Aspect data source:              "none"
Curvature data source:           "none"
LAI data source:                 "LDT"
SAI data source:                 "none"
Albedo data source:              "LDT"
Max snow albedo data source:     "none"
Greenness data source:           "none"  
Roughness data source:           "none"  
Porosity data source:            "none"
Ksat data source:                "none"
B parameter data source:         "none"
Quartz data source:              "none"
Emissivity data source:          "none"

LIS domain and parameter data file:         ../../run_LDT/lis_input.d01.nc
Use greenness fraction climatology: 0
Use albedo climatology: 0
Albedo climatology interval type: "monthly"


#--------------------------------FORCINGS----------------------------------
PRINCETON forcing directory:      ./LIS_PARAMS/FORCING/PRINCE
PRINCETON domain x-dimension size:      360
PRINCETON domain y-dimension size:      180
PRINCETON number of forcing variables:  7


#-----------------------LAND SURFACE MODELS--------------------------
CABLE model timestep:                     30mn
CABLE restart output interval:            24hr
CABLE restart file:                       ../continuous/OUTPUT/SURFACEMODEL/LIS_RST_CABLE_200210300000.d01.nc
CABLE vegetation parameter table:         ./LIS_PARAMS/cable_parms/def_veg_params_igbp_medlyn.txt
CABLE SOILFLAG:                           ssgw  # soilsnow, ssgw
CABLE soil types:                         2 # 1: Zobler, 2: STATSGO
CABLE soil parameter table:               ./LIS_PARAMS/cable_parms/def_soil_params.txt
CABLE hydraulic redistribution:           .FALSE.     # .TRUE., .FALSE.
CABLE wilting parameter:                  0.5         # 
CABLE saturation parameter:               0.8         #
CABLE FWSOIL_SWITCH:                      standard    # standard
                                                      # non-linear extrapolation
                                                      # Lai and Ktaul 2000
CABLE DIAG_SOIL_RESP:                     ON          # ON, OFF
CABLE LEAF_RESPIRATION:                   ON          # ON, OFF
CABLE CONSISTENCY_CHECK:                  .FALSE.     # .TRUE., .FALSE.
CABLE SSNOW_POTEV:                        HDM         # HDM  (Humidity Deficit Method)
                                                      # P-M  (Penman-Monteith)
CABLE RUN_DIAG_LEVEL:                     'BASIC'     # 'BASIC', 'NONE'
CABLE TQ reference height:                40
CABLE UV reference height:                40
CABLE fixed snow-free soil albedo:        0.1
CABLE fixed CO2 concentration:            350.0    # in ppmv
CABLE check mass balance:                 .TRUE.    # .TRUE., .FALSE.
CABLE check energy balance:               .FALSE.   # .TRUE., .FALSE.
CABLE maximum verbosity:                  .true.   # write detail of specified grid cell init and params to log?
CABLE tile to print:                      1758      # tile number to print (0 = print all tiles)

CABLE GS_SWITCH:                          leuning   # leuning, medlyn_fit
CABLE OR_EVAP:                            .TRUE.    # .TRUE., .FALSE.
CABLE L_NEW_ROUGHNESS_SOIL:               .FALSE.    # .TRUE., .FALSE.
CABLE L_NEW_RUNOFF_SPEED:                 .FALSE.    # .TRUE., .FALSE.
CABLE L_NEW_REDUCE_SOILEVP:               .FALSE.    # .TRUE., .FALSE.

#---------------------------MODEL OUTPUT CONFIGURATION-----------------------
#Specify the list of ALMA variables that need to be featured in the 
#LSM model output

Output start year:
Output start month:
Output start day:
Output start hour:
Output start minutes:
Output start seconds:

Output GRIB Table Version: 128
Output GRIB Center Id:     57
Output GRIB Subcenter Id:  2
Output GRIB Process Id:    88
Output GRIB Grid Id:       255

Model output attributes file: './MODEL_OUTPUT_LIST.TBL'
