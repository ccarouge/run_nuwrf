#!/bin/bash
#PBS -l walltime=1:50:00
#PBS -l mem=4GB
#PBS -l ncpus=8
#PBS -j oe
#PBS -q express 
#PBS -l wd

NUWRF_DIR=/short/w35/ccc561/WRF/nu-wrf_v6-3.4.1-p1
source $NUWRF_DIR/raijin.cfg
module load intel-mkl/${EBVERSIONINTELMINFC}
module load arm-forge
module load totalview

#cd /short/w35/ccc561/LIS/LIS7.1/LIS_run/CABLE_1.4

# Create link to LIS data
ln -sf /g/data/w35/LIS/LIS_PARAMS LIS_PARAMS
mpirun -np $PBS_NCPUS ./LIS -f lis.config
#ddt --connect mpirun -np $PBS_NCPUS ./LIS -f lis.config
#mpirun --debug -np 8 ./LIS -f lis.config
