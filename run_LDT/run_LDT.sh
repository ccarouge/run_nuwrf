#!/bin/bash
#PBS -l walltime=2:30:00
#PBS -l mem=15GB
#PBS -l ncpus=1
#PBS -j oe
#PBS -q express
#PBS -l wd
#PBS -l software=intel-cc/2:intel-fc/2
#PBS -W umask=0022

NUWRF_DIR=/short/w35/ccc561/WRF/nuwrf
export PATH="${NUWRF_DIR}/installs/nuwrf_v9_p0:${PATH}"
source $NUWRF_DIR/raijin.cfg

module load intel-mkl/${EBVERSIONINTELMINFC}
module load arm-forge/18.0
module load totalview


LDT ldt.config
