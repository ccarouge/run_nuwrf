#!/bin/bash
#PBS -l walltime=2:30:00
#PBS -l mem=15GB
#PBS -l ncpus=1
#PBS -j oe
#PBS -q express
#PBS -l wd
#PBS -W umask=0022

export NUWRF_DIR=/short/$PROJECT/$USER/WRF/nuwrf
export PATH="${NUWRF_DIR}/installs/nuwrf_debug:${PATH}"
source $NUWRF_DIR/raijin.cfg
module load intel-mkl/${EBVERSIONINTELMINFC}
module load arm-forge/18.0
module load totalview

# Create link to LIS data
ln -sf /g/data/w35/LIS/LIS_PARAMS LIS_PARAMS

#ddt -connect ./LDT ldt.config.prelis
LDT ldt.config.prelis 
