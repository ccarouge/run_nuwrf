#!/bin/bash
#PBS -l walltime=2:30:00
#PBS -l mem=15GB
#PBS -l ncpus=1
#PBS -j oe
#PBS -q express
#PBS -l wd
#PBS -W umask=0022

module purge
module load dot pbs 
module load nco

# Add path to source code
export NUWRF_DIR=/short/$PROJECT/$USER/WRF/nuwrf
if [ -x "${NUWRF_DIR}/installs/nuwrf_v9_p0/LDT" ]; then
    export PATH="${NUWRF_DIR}/installs/nuwrf_v9_p0:${PATH}"
else
    echo "ERROR: LDT not found"
fi

# Post-process the output files. Copy some variables from LDT output 
# to LIS output
LIS_output=`ls OUTPUT/LIS_HIST*`
test_file=LIS_HIST_test
nnest=1

for LIS_output in `ls OUTPUT/LIS_HIST*`
do
    echo Processing nest $dom

    echo cp ${LIS_output} ${test_file}.nc
    cp ${LIS_output} ${test_file}.nc

    echo ncks -A -v TBOT,GREENNESS lis_input.d01.nc ${test_file}.nc
    ncks -A -v TBOT,GREENNESS -d month,2,2 lis_input.d01.nc ${test_file}.nc 
    if [ "$?" -ne "0" ]; then
	   echo "Failed grabbing TBOT and GREENNESS"
	   exit 1
    fi
    
    echo ncwa -C -a month ${test_file}.nc tmp.nc
    ncwa -C -a month ${test_file}.nc tmp.nc
    if [ "$?" -ne "0" ]; then
	    echo "Failed removing month dimension"
	    exit 1
    fi
    mv tmp.nc ${test_file}.nc
    
    echo ncrename -v TBOT,Tempbot_inst -v GREENNESS,Greenness_inst ${test_file}.nc
    ncrename -v TBOT,Tempbot_inst -v GREENNESS,Greenness_inst ${test_file}.nc
    if [ "$?" -ne "0" ]; then
	    echo "Failed renaming variables"
	    exit 1
    fi
    
    ncatted -a scale_factor,Greenness_inst,a,f,1. -a add_offset,Greenness_inst,a,f,0. ${test_file}.nc
    if [ "$?" -ne "0" ]; then
	    echo "Failed adding attributes to variables"
	    exit 1
    fi
done

#ddt -connect ./LDT ldt.config.postlis
LDT ldt.config.postlis 
