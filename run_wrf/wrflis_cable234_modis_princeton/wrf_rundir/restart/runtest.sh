#!/bin/bash
# Runs the CABLE-LIS-WRF test case
# Path to WPS and WRFV3 directories should be passed as WRF_ROOT
# as such:
# export WRF_ROOT=<path to WRF>
# qsub -P $PROJECT -v WRF_ROOT runtest.sh

 
#PBS -q express
#PBS -l walltime=2:15:00
#PBS -l ncpus=4
#PBS -l mem=16gb
#PBS -l wd

set -eu
ulimit -s unlimited

if [ -x "${WRF_ROOT}/installs/nuwrf/real.exe" ]; then
    export PATH="${WRF_ROOT}/installs/nuwrf:${PATH}"
else
    echo "ERROR: WRF not found"
    exit 1
fi
if [ -x "${WRF_ROOT}/installs/nuwrf/geogrid.exe" ]; then
    export PATH="${WRF_ROOT}/installs/nuwrf:${PATH}"
else
    echo "ERROR: WPS not found"
    exit 1
fi

# Need to update to use same openmpi as in compilation
module purge
module load dot pbs
module load openmpi/1.10.7
module load totalview

# Run WPS for ERA-Interim input data
#make WPSDIR=${WRF_ROOT}/WPS 

#Copy namelist file
#cp namelist.input.real namelist.input

#LIS input file
#ln -sf ../lis4real_input.d01.nc .
#real.exe


#Copy namelist file

if [[ -z ${RESTARTOPT} || ${RESTARTOPT} == 0 ]]; then
  echo Not a restart run RESTARTOPT=${RESTARTOPT}
  cp namelist.input.wrf namelist.input
  cp lis.config.wrf lis.config
else
  echo A restart run RESTARTOPT=${RESTARTOPT}
  cp namelist.input.wrf.restart namelist.input
  cp lis.config.wrf.restart lis.config
fi
if [[ -z ${DEBUGOPT} || ${DEBUGOPT} == 0 ]]; then
  echo Not debugging DEBUGOPT=${DEBUGOPT}
  mpirun wrf.exe
else
  echo Debugging call DEBUGOPT=${DEBUGOPT}
  mpirun  wrf.exe.debug
fi
