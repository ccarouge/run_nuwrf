#!/bin/bash
# Runs the CABLE-LIS-WRF test case
# Path to WPS and WRFV3 directories should be passed as WRF_ROOT
# as such:
# export WRF_ROOT=<path to WRF>
# export RESTARTOPT=0   # set to 1 if doing a restart
# export DEBUGOPT=0     # set to 1 to run within a debugger
# qsub -P $PROJECT -v WRF_ROOT,RESTARTOPT,DEBUGOPT runtest.sh
#
# Feel free to simplify running WRF at the end and getting rid of RESTARTOPT and DEBUGOPT
# It was useful for me to test and debug. I've also created an alias for me:
# alias nuqsub='qsub -P w35 -v WRF_ROOT,RESTARTOPT,DEBUGOPT runtest.sh'


 
#PBS -q express
#PBS -l walltime=3:15:00
#PBS -l ncpus=4
#PBS -l mem=8gb
#PBS -l wd
#PBS -l other=gdata1
#PBS -P w97

set -eu
ulimit -s unlimited

if [[ -z ${DEBUGOPT} || ${DEBUGOPT} == 0 ]]; then
  install_dir=nuwrf_v9_p0
else
  install_dir=nuwrf_debug
fi

if [ -x "${WRF_ROOT}/installs/${install_dir}/real.exe" ]; then
    echo "WRF found"
    echo "Exe dir: ${WRF_ROOT}/installs/${install_dir}"
    export PATH="${WRF_ROOT}/installs/${install_dir}:${PATH}"
else
    echo "ERROR: WRF not found"
    exit 1
fi
if [ -x "${WRF_ROOT}/installs/${install_dir}/geogrid.exe" ]; then
    echo "WPS found"
else
    echo "ERROR: WPS not found"
    exit 1
fi

# Need to update to use same openmpi as in compilation
module purge
module load dot pbs
module load openmpi/1.10.7
module load totalview

# Run WPS for ERA-Interim input data
make WPSDIR=${WRF_ROOT}/WPS 

#Copy namelist file
cp namelist.input.real namelist.input

#LIS input file
ln -sf ../lis4real_input.d01.nc .
real.exe


##Copy namelist file
#
#if [[ -z ${RESTARTOPT} || ${RESTARTOPT} == 0 ]]; then
#  echo Not a restart run RESTARTOPT=${RESTARTOPT}
#  cp namelist.input.wrf namelist.input
#  cp lis.config.wrf lis.config
#else
#  echo A restart run RESTARTOPT=${RESTARTOPT}
#  cp namelist.input.wrf.restart namelist.input
#  cp lis.config.wrf.restart lis.config
#fi
#if [[ -z ${DEBUGOPT} || ${DEBUGOPT} == 0 ]]; then
#  echo Not debugging DEBUGOPT=${DEBUGOPT}
#  mpirun wrf.exe
#else
#  echo Debugging call DEBUGOPT=${DEBUGOPT}
#  mpirun --debug wrf.exe
#fi
