#!/bin/bash
#PBS -l walltime=1:50:00
#PBS -l mem=48GB
#PBS -l ncpus=96
#PBS -j oe
#PBS -q express 
#PBS -l wd
#PBS -W umask=0022

NUWRF_DIR=/short/$PROJECT/$USER/WRF/nuwrf
export PATH="${NUWRF_DIR}/installs/nuwrf_debug:${PATH}"
source $NUWRF_DIR/raijin.cfg
module load intel-mkl/${EBVERSIONINTELMINFC}
module load allinea-forge
module load totalview

#cd /short/w35/ccc561/LIS/LIS7.1/LIS_run/CABLE_1.4

# Create link to LIS data
ln -sf /g/data/w35/LIS/LIS_PARAMS LIS_PARAMS

#mpiexec -np $PBS_NCPUS ./LIS -f lis.config.test
#mpirun -np $PBS_NCPUS LIS -f lis.config.debug
mpirun -np $PBS_NCPUS LIS -f lis.config.coldstart
#mpirun --debug -np 16 ./LIS -f lis.config.coldstart
