
The PRINCETON Forcing Elevation File

The Princeton forcing elevation file consists of using Hydro1k elevation
data to downscale/upscale to the Princeton reanalysis forcing domain to
derive the reanalysis forcing height map.  This map is provided to perform
topographic lapse-rate adjustments to the original Princeton reanalysis
forcing fields.  For more information, please see and cite the reference
below.

Reference:
Sheffield, J., G. Goteti, and E. F. Wood, 2006: Development of a 50-yr high-resolution 
global dataset of meteorological forcings for land surface modeling, 
J. Climate, 19 (13), 3088-3111.

Acknowledgement for providing this dataset:
Justin Sheffield, at Princeton University.

